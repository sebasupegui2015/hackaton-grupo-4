const mongoose = require('mongoose');
const url_db = process.env.URL_DB

module.exports = async () => {
    const con = await mongoose.connect(
        url_db,
        {useNewUrlParser: true, useUnifiedTopology: true})
    .then((result) => console.log('Success connect MongoDB'))
    .catch((error) => console.log(error))
}
