const express = require('express');
const cors = require('cors');
const path = require('path');
require('dotenv').config();
const con = require('./database/conexion');
const controllerComercio = require('./comercios/controller');
const controlerInventario = require('./inventarios/controller');
const controllerGeoJson = require('./geoJSON/controller');
//Api config
const app = express();
const port = process.env.APP_PORT;
app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use(cors());

// Configuirar carpeta publica
const publicPath = path.resolve(__dirname, '');
app.use(express.static(publicPath));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + './index.html'));
});

// Routes comercio
app.use('/comercios', controllerComercio);

// Routes inventario
app.use("/inventarios", controlerInventario);


// GeoJson
app.use("/geojson", controllerGeoJson);

// Servidor
app.listen(port, () => {
    console.log("App coriendo en el puerto: " + port);
})

con();
