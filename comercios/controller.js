const express = require('express');
const routes = express.Router();

const serviciosComercio = require('./services');

routes.get('/', serviciosComercio.getComercios);
routes.get('/:id', serviciosComercio.getComercio);
routes.post('/crear', serviciosComercio.createComercio);
routes.put('/editar/:id', serviciosComercio.updateComercio);
routes.delete('/eliminar/:id', serviciosComercio.deleteComcercio);

module.exports = routes;

