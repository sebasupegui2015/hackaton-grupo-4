const modelComercio = require('./model');

module.exports.getComercios = (req, res) => {
    try {
        modelComercio.find({} , (error, docs) =>{
            if(error){
                res.send({'error': error});
            } else{
                res.send(docs);
            }
        })
    } catch (error) {
        console.log(error);
    }
}

module.exports.getComercio = (req, res) => {
    const id = req.params.id;
    try {
        modelComercio.find({"id": id} , (error, docs) =>{
            if(error){
                res.send({'error': error});
            } else{
                res.send(docs);
            }
        })
    } catch (error) {
        console.log(error);
    }
}

module.exports.createComercio = (req, res) =>{
    const datos = req.body;
    try {
        modelComercio.create(datos, (error, docs) => {
            if(error){
                res.send(error);
            } else {
                res.send(docs);
            }
        })
    } catch (error) {
        console.log(error);
    }
}

module.exports.updateComercio = (req, res) => {
    const id = req.params.id;
    const datos = req.body;

    try {
        modelComercio.updateOne({id:id}, datos, (error, docs) =>{
            if(error){
                res.send(error);
            } else {
                res.send(docs);
            }
        });
    } catch (error) {
        console.log(error);;
    }
}

module.exports.deleteComcercio = (req, res) => {
    const id = req.params.id;
    try {
        modelComercio.deleteOne({id:id}, (error, docs) => {
            if(error){
                res.send(error);
            }else{
                res.send(docs);
            }
        })
    } catch (error) {
        console.log(error);
    }
}
