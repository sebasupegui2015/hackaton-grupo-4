const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const geoJsonSchema = new Schema({
        id:{
            type:Number,
            required: true,
            unique: true
        },
        nombre:{
            type:String,
            required: true,
        },
        direccion:{
            type:String,
            required: true
        },
        descripcion:{
            type:String,
            required: true
        },
        contacto:{
            type:String,
            required: true
        },
        correo:{
            type:String,
            required: true
        },
        departamento:{
            type:String,
            required: true
        },
        ciudad:{
            type:String,
            required: true
        },
        //Latitud y longitud
        coordinates:{
            type:Array,
            required: true
        },
        docs_inventario:{
            type:Array,
            required: true
        }
    },
    {
        // Evitar que envie el número de la versión a la base de datos
        versionKey: false,
    }
);

const geoJSON = mongoose.model('geoJSON', geoJsonSchema, "geoJSON");
module.exports =  geoJSON;