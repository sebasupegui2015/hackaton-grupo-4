const modelGeoJson = require('./model');

module.exports.getGeoJson = (req, res) => {
    try {
        modelGeoJson.find({} , (error, docs) =>{
            if(error){
                res.send({'error': error});
            } else{
                res.send(docs);
            }
        })
    } catch (error) {
        res.send({'error': error})
    }
}