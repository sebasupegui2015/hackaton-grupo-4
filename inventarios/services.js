const modelInventario = require('./model');

module.exports.getInventarios = (req, res) => {
    try {
        modelInventario.find({} , (error, docs) =>{
            if(error){
                res.send({'error': error});
            } else{
                res.send(docs);
            }
        })
    } catch (error) {
        res.send({'error': error})
    }
}

module.exports.getInventario = (req, res) => {
    const id = req.params.id;
    try {
        modelInventario.find({"id_comercio": id} , (error, docs) =>{
            if(error){
                res.send({'error': error});
            } else{
                res.send(docs);
            }
        })
    } catch (error) {
        res.send({'error': error})
    }
}

module.exports.createInventario = (req, res) =>{
    const datos = req.body;
    try {
        modelInventario.create(datos, (error, docs) => {
            if(error){
                res.send(error);
            } else {
                res.send(docs);
            }
        })
    } catch (error) {
        console.log(error);
    }
}

module.exports.updateInventario = (req, res) => {
    const id = req.params.id;
    const datos = req.body;

    try {
        modelInventario.updateOne({id_producto:id}, datos, (error, docs) =>{
            if(error){
                res.send(error);
            } else {
                res.send(docs);
            }
        });
    } catch (error) {
        res.send(error);
    }
}

module.exports.deleteInventario = (req, res) => {
    const id = req.params.id;
    try {
        modelInventario.deleteOne({id_producto:id}, (error, docs) => {
            if(error){
                res.send(error);
            }else{
                res.send(docs);
            }
        })
    } catch (error) {
        console.log(error);
    }
}