const express = require('express');
const routes = express.Router();

const serviciosInventario = require('./services');

routes.get('/', serviciosInventario.getInventarios);
routes.get('/:id', serviciosInventario.getInventario);
routes.post('/crear', serviciosInventario.createInventario);
routes.put('/editar/:id', serviciosInventario.updateInventario);
routes.delete('/eliminar/:id', serviciosInventario.deleteInventario);

module.exports = routes;